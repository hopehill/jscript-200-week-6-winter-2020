$(document).ready(function() {

// Create a new <a> element containing the text "Buy Now!"
// with an id of "cta" after the last <p>

console.log($('<a>');
$('<a>').text('Buy Now!');
$('<a>').attr('id', 'cta');
$('p').last().remove();


// Access (read) the data-color attribute of the <img>,
// log to the console
console.log($('img'));
$('img').css('color');

// Update the third <li> item ("Turbocharged"),
// set the class name to "highlight"

console.log($('li'));
$('li').addClass('highlight');

// Remove (delete) the last paragraph
// (starts with "Available for purchase now…")


// Create a listener on the "Buy Now!" link that responds to a click event.
// When clicked, the the "Buy Now!" link should be removed
// and replaced with text that says "Added to cart"
const $a = $('<a>');
$a.text('Buy Now!');
$a.attr('id', 'cta');
$(',ain').append($a);

$('#cta').click(function(e) {
	$(this).remove();
	const $p = $('<p>');
	$p.text('Added to cart');
	$main.append($p);
});

$('ul').on('click', 'li', function() {
	console.log(this);
	const currentHtml = $(this).html();
	$(this).html(currentHtml + ' clk');
});

